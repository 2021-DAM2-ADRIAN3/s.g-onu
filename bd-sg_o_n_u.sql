-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 30-11-2020 a las 19:54:18
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bd-sg o.n.u`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `edifici`
--

CREATE TABLE `edifici` (
  `ID_EDIFICI` varchar(50) NOT NULL,
  `REGIO` varchar(50) NOT NULL,
  `QUANTITAT_HABITANTS` int(11) NOT NULL,
  `CATEGORIA` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `edifici`
--

INSERT INTO `edifici` (`ID_EDIFICI`, `REGIO`, `QUANTITAT_HABITANTS`, `CATEGORIA`) VALUES
('A1', 'VERDNORD', 5000, 1),
('A2', 'BLAUSUD', 7000, 2),
('A1', 'VERDNORD', 5000, 1),
('A2', 'BLAUSUD', 7000, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gremi`
--

CREATE TABLE `gremi` (
  `NOM` varchar(50) NOT NULL,
  `REGIO` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `gremi`
--

INSERT INTO `gremi` (`NOM`, `REGIO`) VALUES
('OBRER', 'BLAUSUD'),
('METGE', 'VERDNORT'),
('OBRER', 'BLAUSUD'),
('METGE', 'VERDNORT');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `habitant`
--

CREATE TABLE `habitant` (
  `ID` varchar(11) NOT NULL,
  `GREMI` varchar(50) NOT NULL,
  `REGIO` varchar(50) NOT NULL,
  `EDIFICI` varchar(20) NOT NULL,
  `EDAT` int(2) NOT NULL,
  `GRUPSANGUINI` varchar(5) NOT NULL,
  `RAÇA` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `habitant`
--

INSERT INTO `habitant` (`ID`, `GREMI`, `REGIO`, `EDIFICI`, `EDAT`, `GRUPSANGUINI`, `RAÇA`) VALUES
('O27A1123456', 'OBRER', 'BLAUSUD', 'A1', 25, 'A+', 'ASIATIC'),
('M28A2123465', 'METGE', 'VERDNORT', 'A2', 30, 'B-', 'EUROPEU');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `regio`
--

CREATE TABLE `regio` (
  `NOM` varchar(50) NOT NULL,
  `EXTENSIO` varchar(50) NOT NULL,
  `GREMISREGIO` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `regio`
--

INSERT INTO `regio` (`NOM`, `EXTENSIO`, `GREMISREGIO`) VALUES
('VERDNORT', '10KM2', 'METGE'),
('BLAUSUD', '50KM2', 'OBRER'),
('VERDNORT', '10KM2', 'METGE'),
('BLAUSUD', '50KM2', 'OBRER');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
