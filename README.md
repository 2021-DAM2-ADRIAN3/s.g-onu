#  **SG ONU**

### EL PROJECTE DEL SISTEMA DE GESTIO DE LA ONU(CYBERPUNK ), CONSTARA DE DOS PARTS:

## FORM 1
### EN ESTE FORMULARIO ENCONTRAMOS LA PARTE PRINCIPAL DEL PROGRAMA, QUE ES LA DE LA BUSQUEDA DE LOS HABITANTES, MEDIANTE UN id UNICO PARA CADA UNO, LA BUSCA DE LOS DIFERENTES GREMIOS Y REGIONES SEGUN EL NOMBRE Y LA BUSQUEDA DE EDIFICIOS SEGÚN SU CODIGO.
Para hacer las busquedas, seguiremos el mismo patron siempre, insertar el que se nos indica y después hacer clic el el boton de buscar de cada apartado.

1. **HABITANTE**--> El "id"estará formado por una letra al principio, que será la primera letra con la que empieza el nombre de su Gremio, dos números a seguir, que serán los Id de cada región, una letra y un número que serán los Id de los edificios de cada re-gión y para finalizar un número de como máximo 6 dígitos que será aleatorio pero único para cada persona Ex "O27A1123456" Se mostrara de cada habitante la siguiente informacion: la edat, la raza,grupo sanguinio, la region a la que pertenecen, el gremio y el edificio donde viven.
2. **GREMIO**--> En el gremio, solo hará falta poner el nombre del gremio, i nos saldrá la región donde se encuentra trabajando ese gremio de personas
3. **REGIÓN**--> En la region, para buscar, escribiremos el nombre de la región de la cual queremos saber datos, nos mostraráa la extension de la region y los gremios que hay en la region´.
4. **EDIFICIO**--> Para buscar un edificio en concreto, pondremos su identificador  unico, nos mostraráa la cantidad de habitantes que lo ocupan,  la categoria del edificio(hospital, taller etc...), y por ultimo  la region en la que se encuentra.

**BOTON DE  AÑADIR DATOS**-->Haremos click en este botón cuando tengamos la necesidad de ñadir datos en nuestra base de datos, una vez hagamos click nos redigirà al formulario numero 2 que se explica a continuación:

##**FORMULARIO 2**
**EN ESTE FORMULARIO HAREMOS EL INSERT DE DATOS PERO SOLO DE PERSONAS, YA QUE SON LOS DATOS QUE MAS SE MODIFICAN, PUESTO QUE LAS REGIONES, EDIFICIOS Y GREMIOS NO CAMBIAN HABITUALMENTE. INSERTAREMOS LOS DATOS QUE SE NOS PIDE I HAREMOS CLICK EN EL BOTON DE INSERTAR PERSONA, SI QUEREMOS VOLVER AL FORMULARIO 1 PARA HACER UNA BUSQUEDA, SOLO TENDREMOS QUE HACER CLICK EN EL BOTON DE "BUSCAR", COLOCADO EN LA PARTE SUPERIOR DERECHA**