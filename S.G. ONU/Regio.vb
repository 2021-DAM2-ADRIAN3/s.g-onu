﻿Public Class Regio
    'a la clase regio ens podem trobar el nom de la regio, la extensio i els gremis que la conformen'
    Private NOM_REGIO As String
    Private EXTENSIO_REGIO As String
    Private GREMIS_REGIO As String


    'creem el constructor que rep per paramentre lo abans comentat'
    Public Sub New(ByVal NOM_REGIO As String, ByVal EXTENSIO_REGIO As String, ByVal GREMIS_REGIO As String)
        Me.NOM_REGIO = NOM_REGIO
        Me.EXTENSIO_REGIO = EXTENSIO_REGIO
        Me.GREMIS_REGIO = GREMIS_REGIO
    End Sub
    'creem tots els getters i els setters'
    Public Sub setNOM_REGIO(ByVal NOM_REGIO As String)
        Me.NOM_REGIO = NOM_REGIO
    End Sub

    Public Function getNOM_REGIO() As String
        Return Me.NOM_REGIO
    End Function

    Public Sub setEXTENSIO_REGIO(ByVal EXTENSIO_REGIO As String)
        Me.EXTENSIO_REGIO = EXTENSIO_REGIO
    End Sub
    Public Function getEXTENSIO_REGIO() As String
        Return Me.EXTENSIO_REGIO
    End Function

    Public Sub setGREMIS_REGIO(ByVal GREMIS_REGIO As String)
        Me.GREMIS_REGIO = GREMIS_REGIO
    End Sub
    Public Function getGREMIS_REGIO() As String
        Return Me.GREMIS_REGIO
    End Function
End Class