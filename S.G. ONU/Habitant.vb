﻿Public Class Habitant
    ' a la clase habitant ens trobem amb la edat de la persona, el edifici on viu, el gremi al que pertany,
    'grup sanguina al que pertany, el seu ID unic, la ´que es i la regio on viu.
    Private EDAT As Int32
    Private EDIFICI As String
    Private GREMI As String
    Private GRUPSANGUINI As String
    Private ID As String
    Private RAÇA As String
    Private REGIO As String

    'creem el constructor'
    Public Sub New(ByVal EDAT As Int32, ByVal EDIFICI As String, ByVal GREMI As String, ByVal GRUPSANGUINI As String,
                   ByVal ID As String, ByVal RAÇA As String, ByVal REGIO As String)
        Me.EDAT = EDAT
        Me.EDIFICI = EDIFICI
        Me.GREMI = GREMI
        Me.GRUPSANGUINI = GRUPSANGUINI
        Me.ID = ID
        Me.RAÇA = RAÇA
        Me.REGIO = REGIO
    End Sub
    'creem getters i setters'
    Public Sub setEDAT(ByVal EDAT As Int32)
        Me.EDAT = EDAT
    End Sub

    Public Function getEDAT() As String
        Return Me.EDAT
    End Function

    Public Sub setEDIFICI(ByVal EDIFICI As String)
        Me.EDIFICI = EDIFICI
    End Sub

    Public Function getEDIFICI() As String
        Return Me.EDIFICI
    End Function

    Public Sub setGREMI(ByVal GREMI As String)
        Me.GREMI = GREMI
    End Sub

    Public Function getGREMI() As String
        Return Me.GREMI
    End Function

    Public Sub setGRUPSANGUINI(ByVal GRUPSANGUINI As String)
        Me.GRUPSANGUINI = GRUPSANGUINI
    End Sub

    Public Function getGRUPSANGUINI() As String
        Return Me.GRUPSANGUINI
    End Function
    Public Sub setID(ByVal ID As String)
        Me.ID = ID
    End Sub

    Public Function getID() As String
        Return Me.ID
    End Function
    Public Sub setRAÇA(ByVal RAÇA As String)
        Me.RAÇA = RAÇA
    End Sub

    Public Function getRAÇA() As String
        Return Me.RAÇA
    End Function
    Public Sub setREGIO(ByVal REGIO As String)
        Me.REGIO = REGIO
    End Sub

    Public Function getREGIO() As String
        Return Me.REGIO
    End Function



End Class