﻿
Imports MySql.Data.MySqlClient
'fem el import de la base de dades creada'
Public Class Form1
    'conectem la nostra base de dades amb el projecte'
    Dim connexio As MySqlConnection
    Dim query As String

    'quan es pitji el boto buscarHABITANT, el que estarem fent serà cridar a la funcio mostrarHabitant
    'pero a part, tambe es conectara i desconectara dela base de dades, el funcionament serà el mateix per els 
    'botons de buscar gremi, regio i edifici
    Private Sub BUSCARHABITANT_Click(sender As Object, e As EventArgs) Handles BUSCARHABITANT.Click

        connectar()
        mostrarHabitant()
        desconnectar()
    End Sub

    Private Sub BUSCARGREMI_Click(sender As Object, e As EventArgs) Handles BUSCARGREMI.Click
        connectar()
        mostrarGremi()
        desconnectar()
    End Sub

    Private Sub BUSCARREGIO_Click(sender As Object, e As EventArgs) Handles BUSCARREGIO.Click

        connectar()
        mostrarRegio()
        desconnectar()
    End Sub
    Private Sub BUSCAREDIFICI_Click(sender As Object, e As EventArgs) Handles BUSCAREDIFICI.Click

        connectar()
        mostrarEdifici()
        desconnectar()
    End Sub

    'a les funcions, el funcionament serà el mateix per totes:
    Private Sub mostrarGremi()
        query = $"Select * FROM `Gremi` WHERE `NOM` Like '{TextBox2.Text}'" 'seleccionem de la taula gremi,el atribut que utilitzarem per fer la busca al textbox(al que li fiquem per teclat
        'passem les dades de la base de dades a una taula de visual basic'
        Dim comanda As New MySqlCommand(query, connexio)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)

        Dim Gremi As New Gremi("", "")
        Gremi.setNOM_GREMI(conjunt_dades.Rows(0).Item(0)) 'agafem els valors de la fila 0, columna 1
        Gremi.setREGIO_GREMI(conjunt_dades.Rows(0).Item(1)) 'agafem els valors de la fila 0, columna 2


        'mostrem els valors a les etiquetes'
        ETIQUETA_NOM_GREMI.Text = Gremi.getNOM_GREMI()
        ETIQUETA_REGIO_GREMI.Text = Gremi.getREGIO_GREMI()

        desconnectar()
    End Sub
    Private Sub mostrarEdifici()
        query = $"Select * FROM `Edifici` WHERE `ID_EDIFICI` Like '{TextBox4.Text}'"
        'PASSAR DADES DE LA BD A UNA TAULA DE DADES DE VB'
        Dim comanda As New MySqlCommand(query, connexio)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)

        Dim Edifici As New Edifici("", "", 0, 0)
        Edifici.setID_EDIFICI(conjunt_dades.Rows(0).Item(0)) 'agafem els valors de la fila 0, columna 1
        Edifici.setREGIO_EDIFICI(conjunt_dades.Rows(0).Item(1)) 'agafem els valors de la fila 0, columna 2
        Edifici.setQUANTITAT_HABITANTS_EDIFICI(conjunt_dades.Rows(0).Item(2)) 'agafem els valors de la fila 0, columna 3
        Edifici.setCATEGORIA_EDIFICI(conjunt_dades.Rows(0).Item(3)) 'agafem els valors de la fila 0, columna 4

        'mostrem els valors a les etiquetes
        ETIQUETA_ID_EDIFICI.Text = Edifici.getID_EDIFICI()
        ETIQUETA_REGIO_EDIFICI.Text = Edifici.getREGIO_EDIFICI()
        ETIQUETA_QUANTITAT_HABITANTS_EDIFICI.Text = Edifici.getQUANTITAT_HABITANTS_EDIFICI()
        ETIQUETA_CATEGORIA_EDIFICI.Text = Edifici.getCATEGORIA_EDIFICI()

        desconnectar()
    End Sub
    Private Sub mostrarRegio()
        query = $"Select * FROM `Regio` WHERE `NOM` Like '{TextBox3.Text}'"
        'PASSAR DADES DE LA BD A UNA TAULA DE DADES DE VB'
        Dim comanda As New MySqlCommand(query, connexio)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)

        Dim Regio As New Regio("", 0, "")
        Regio.setNOM_REGIO(conjunt_dades.Rows(0).Item(0)) 'agafem els valors de la fila 0, columna 1
        Regio.setEXTENSIO_REGIO(conjunt_dades.Rows(0).Item(1)) 'agafem els valors de la fila 0, columna 2
        Regio.setGREMIS_REGIO(conjunt_dades.Rows(0).Item(2)) 'agafem els valors de la fila 0, columna 3


        'mostrem els valors a les etiquetes
        ETIQUETA_NOM_REGIO.Text = Regio.getNOM_REGIO()
        ETIQUETA_EXTENSIO_REGIO.Text = Regio.getEXTENSIO_REGIO()
        ETIQUETA_GREMIS_REGIO.Text = Regio.getGREMIS_REGIO()


        desconnectar()
    End Sub

    Private Sub mostrarHabitant()
        query = $"Select * FROM `habitant` WHERE `ID` Like '{TextBox1.Text}'"
        'PASSAR DADES DE LA BD A UNA TAULA DE DADES DE VB'
        Dim comanda As New MySqlCommand(query, connexio)
        Dim adaptador As New MySqlDataAdapter(comanda)
        Dim conjunt_dades As New DataTable()
        adaptador.Fill(conjunt_dades)

        Dim Habitant As New Habitant(0, "", "", "", "", "", "")
        Habitant.setEDAT(conjunt_dades.Rows(0).Item(4)) 'agafem els valors de la fila 0, columna 1
        Habitant.setEDIFICI(conjunt_dades.Rows(0).Item(3)) 'agafem els valors de la fila 0, columna 2
        Habitant.setGREMI(conjunt_dades.Rows(0).Item(1)) 'agafem els valors de la fila 0, columna 2
        Habitant.setGRUPSANGUINI(conjunt_dades.Rows(0).Item(5)) 'agafem els valors de la fila 0, columna 2
        Habitant.setID(conjunt_dades.Rows(0).Item(0)) 'agafem els valors de la fila 0, columna 2
        Habitant.setRAÇA(conjunt_dades.Rows(0).Item(6)) 'agafem els valors de la fila 0, columna 2
        Habitant.setREGIO(conjunt_dades.Rows(0).Item(2)) 'agafem els valors de la fila 0, columna 2

        'mostrem els valors a les etiquetes
        ETIQUETA_EDAT.Text = Habitant.getEDAT()
        ETIQUETA_EDIFICI.Text = Habitant.getEDIFICI()
        ETIQUETA_GREMI.Text = Habitant.getGREMI()
        ETIQUETA_GRUPSANGUINI.Text = Habitant.getGRUPSANGUINI()
        ETIQUETA_ID.Text = Habitant.getID()
        ETIQUETA_RAÇA.Text = Habitant.getRAÇA()
        ETIQUETA_REGIO.Text = Habitant.getREGIO()

        desconnectar()
    End Sub

    'a la funcio conectar, el que fem es buscar la ruta de la nostra base de dades, es a dir on està allotjada.
    Private Sub connectar()
        Try
            connexio = New MySqlConnection()
            connexio.ConnectionString = "server=localhost;user id=root;password=;database=bd-sg o.n.u"
            connexio.Open()
            MessageBox.Show("Connectat")
        Catch
            MessageBox.Show("Error")
        End Try



    End Sub
    'funcio per desconectar'
    Private Sub desconnectar()
        connexio.Close()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Hide()
        Form2.Show()
    End Sub
End Class
