﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form2
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form2))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.INSERTARPERSONA = New System.Windows.Forms.Button()
        Me.INSERTARID = New System.Windows.Forms.Button()
        Me.INSERTAREDAT = New System.Windows.Forms.Button()
        Me.INSERTARRAÇA = New System.Windows.Forms.Button()
        Me.INSERTARGRUPSANGUINI = New System.Windows.Forms.Button()
        Me.INSERTARREGIO = New System.Windows.Forms.Button()
        Me.INSERTARGRAMI = New System.Windows.Forms.Button()
        Me.INSERTAREDIFICI = New System.Windows.Forms.Button()
        Me.TEXTID = New System.Windows.Forms.TextBox()
        Me.TEXTEDAT = New System.Windows.Forms.TextBox()
        Me.TEXTRAÇA = New System.Windows.Forms.TextBox()
        Me.TEXTGRUPSANGUINI = New System.Windows.Forms.TextBox()
        Me.TEXTREGIO = New System.Windows.Forms.TextBox()
        Me.TEXTGREMI = New System.Windows.Forms.TextBox()
        Me.TEXTEDIFICI = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Lime
        Me.Button1.Font = New System.Drawing.Font("Arial Black", 15.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Button1.ForeColor = System.Drawing.SystemColors.MenuText
        Me.Button1.Location = New System.Drawing.Point(331, -1)
        Me.Button1.Name = "Button1"
        Me.Button1.Padding = New System.Windows.Forms.Padding(0, 0, 0, 3)
        Me.Button1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button1.Size = New System.Drawing.Size(144, 70)
        Me.Button1.TabIndex = 1
        Me.Button1.Text = "S.G ONU"
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.Button1.UseVisualStyleBackColor = False
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(646, 16)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(93, 43)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "CERCA"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'INSERTARPERSONA
        '
        Me.INSERTARPERSONA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.INSERTARPERSONA.Location = New System.Drawing.Point(38, 254)
        Me.INSERTARPERSONA.Name = "INSERTARPERSONA"
        Me.INSERTARPERSONA.Size = New System.Drawing.Size(157, 69)
        Me.INSERTARPERSONA.TabIndex = 4
        Me.INSERTARPERSONA.Text = "INSERTAR PERSONA"
        Me.INSERTARPERSONA.UseVisualStyleBackColor = True
        '
        'INSERTARID
        '
        Me.INSERTARID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.INSERTARID.Location = New System.Drawing.Point(224, 170)
        Me.INSERTARID.Name = "INSERTARID"
        Me.INSERTARID.Size = New System.Drawing.Size(96, 27)
        Me.INSERTARID.TabIndex = 5
        Me.INSERTARID.Text = "ID"
        Me.INSERTARID.UseVisualStyleBackColor = True
        '
        'INSERTAREDAT
        '
        Me.INSERTAREDAT.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.INSERTAREDAT.Location = New System.Drawing.Point(224, 203)
        Me.INSERTAREDAT.Name = "INSERTAREDAT"
        Me.INSERTAREDAT.Size = New System.Drawing.Size(96, 26)
        Me.INSERTAREDAT.TabIndex = 6
        Me.INSERTAREDAT.Text = "EDAT"
        Me.INSERTAREDAT.UseVisualStyleBackColor = True
        '
        'INSERTARRAÇA
        '
        Me.INSERTARRAÇA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.INSERTARRAÇA.Location = New System.Drawing.Point(224, 235)
        Me.INSERTARRAÇA.Name = "INSERTARRAÇA"
        Me.INSERTARRAÇA.Size = New System.Drawing.Size(96, 26)
        Me.INSERTARRAÇA.TabIndex = 7
        Me.INSERTARRAÇA.Text = "RAÇA"
        Me.INSERTARRAÇA.UseVisualStyleBackColor = True
        '
        'INSERTARGRUPSANGUINI
        '
        Me.INSERTARGRUPSANGUINI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.INSERTARGRUPSANGUINI.Location = New System.Drawing.Point(224, 267)
        Me.INSERTARGRUPSANGUINI.Name = "INSERTARGRUPSANGUINI"
        Me.INSERTARGRUPSANGUINI.Size = New System.Drawing.Size(96, 43)
        Me.INSERTARGRUPSANGUINI.TabIndex = 8
        Me.INSERTARGRUPSANGUINI.Text = "GRUP SANGUINI"
        Me.INSERTARGRUPSANGUINI.UseVisualStyleBackColor = True
        '
        'INSERTARREGIO
        '
        Me.INSERTARREGIO.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.INSERTARREGIO.Location = New System.Drawing.Point(224, 316)
        Me.INSERTARREGIO.Name = "INSERTARREGIO"
        Me.INSERTARREGIO.Size = New System.Drawing.Size(96, 32)
        Me.INSERTARREGIO.TabIndex = 9
        Me.INSERTARREGIO.Text = "REGIO"
        Me.INSERTARREGIO.UseVisualStyleBackColor = True
        '
        'INSERTARGRAMI
        '
        Me.INSERTARGRAMI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.INSERTARGRAMI.Location = New System.Drawing.Point(224, 354)
        Me.INSERTARGRAMI.Name = "INSERTARGRAMI"
        Me.INSERTARGRAMI.Size = New System.Drawing.Size(96, 27)
        Me.INSERTARGRAMI.TabIndex = 10
        Me.INSERTARGRAMI.Text = "GREMI"
        Me.INSERTARGRAMI.UseVisualStyleBackColor = True
        '
        'INSERTAREDIFICI
        '
        Me.INSERTAREDIFICI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.INSERTAREDIFICI.Location = New System.Drawing.Point(224, 387)
        Me.INSERTAREDIFICI.Name = "INSERTAREDIFICI"
        Me.INSERTAREDIFICI.Size = New System.Drawing.Size(96, 27)
        Me.INSERTAREDIFICI.TabIndex = 11
        Me.INSERTAREDIFICI.Text = "EDIFICI"
        Me.INSERTAREDIFICI.UseVisualStyleBackColor = True
        '
        'TEXTID
        '
        Me.TEXTID.Location = New System.Drawing.Point(318, 172)
        Me.TEXTID.Name = "TEXTID"
        Me.TEXTID.Size = New System.Drawing.Size(184, 20)
        Me.TEXTID.TabIndex = 12
        '
        'TEXTEDAT
        '
        Me.TEXTEDAT.Location = New System.Drawing.Point(318, 207)
        Me.TEXTEDAT.Name = "TEXTEDAT"
        Me.TEXTEDAT.Size = New System.Drawing.Size(184, 20)
        Me.TEXTEDAT.TabIndex = 13
        '
        'TEXTRAÇA
        '
        Me.TEXTRAÇA.Location = New System.Drawing.Point(318, 239)
        Me.TEXTRAÇA.Name = "TEXTRAÇA"
        Me.TEXTRAÇA.Size = New System.Drawing.Size(184, 20)
        Me.TEXTRAÇA.TabIndex = 14
        '
        'TEXTGRUPSANGUINI
        '
        Me.TEXTGRUPSANGUINI.Location = New System.Drawing.Point(318, 279)
        Me.TEXTGRUPSANGUINI.Name = "TEXTGRUPSANGUINI"
        Me.TEXTGRUPSANGUINI.Size = New System.Drawing.Size(184, 20)
        Me.TEXTGRUPSANGUINI.TabIndex = 15
        '
        'TEXTREGIO
        '
        Me.TEXTREGIO.Location = New System.Drawing.Point(318, 323)
        Me.TEXTREGIO.Name = "TEXTREGIO"
        Me.TEXTREGIO.Size = New System.Drawing.Size(184, 20)
        Me.TEXTREGIO.TabIndex = 16
        '
        'TEXTGREMI
        '
        Me.TEXTGREMI.Location = New System.Drawing.Point(318, 358)
        Me.TEXTGREMI.Name = "TEXTGREMI"
        Me.TEXTGREMI.Size = New System.Drawing.Size(184, 20)
        Me.TEXTGREMI.TabIndex = 17
        '
        'TEXTEDIFICI
        '
        Me.TEXTEDIFICI.Location = New System.Drawing.Point(318, 391)
        Me.TEXTEDIFICI.Name = "TEXTEDIFICI"
        Me.TEXTEDIFICI.Size = New System.Drawing.Size(184, 20)
        Me.TEXTEDIFICI.TabIndex = 18
        '
        'Form2
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.TEXTEDIFICI)
        Me.Controls.Add(Me.TEXTGREMI)
        Me.Controls.Add(Me.TEXTREGIO)
        Me.Controls.Add(Me.TEXTGRUPSANGUINI)
        Me.Controls.Add(Me.TEXTRAÇA)
        Me.Controls.Add(Me.TEXTEDAT)
        Me.Controls.Add(Me.TEXTID)
        Me.Controls.Add(Me.INSERTAREDIFICI)
        Me.Controls.Add(Me.INSERTARGRAMI)
        Me.Controls.Add(Me.INSERTARREGIO)
        Me.Controls.Add(Me.INSERTARGRUPSANGUINI)
        Me.Controls.Add(Me.INSERTARRAÇA)
        Me.Controls.Add(Me.INSERTAREDAT)
        Me.Controls.Add(Me.INSERTARID)
        Me.Controls.Add(Me.INSERTARPERSONA)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form2"
        Me.Text = "Form2"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents INSERTARPERSONA As Button
    Friend WithEvents INSERTARID As Button
    Friend WithEvents INSERTAREDAT As Button
    Friend WithEvents INSERTARRAÇA As Button
    Friend WithEvents INSERTARGRUPSANGUINI As Button
    Friend WithEvents INSERTARREGIO As Button
    Friend WithEvents INSERTARGRAMI As Button
    Friend WithEvents INSERTAREDIFICI As Button
    Friend WithEvents TEXTID As TextBox
    Friend WithEvents TEXTEDAT As TextBox
    Friend WithEvents TEXTRAÇA As TextBox
    Friend WithEvents TEXTGRUPSANGUINI As TextBox
    Friend WithEvents TEXTREGIO As TextBox
    Friend WithEvents TEXTGREMI As TextBox
    Friend WithEvents TEXTEDIFICI As TextBox
End Class
