﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Button1 = New System.Windows.Forms.Button()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.BUSCARHABITANT = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.ETIQUETA_EDAT = New System.Windows.Forms.Label()
        Me.ETIQUETA_GREMI = New System.Windows.Forms.Label()
        Me.ETIQUETA_EDIFICI = New System.Windows.Forms.Label()
        Me.ETIQUETA_GRUPSANGUINI = New System.Windows.Forms.Label()
        Me.ETIQUETA_ID = New System.Windows.Forms.Label()
        Me.ETIQUETA_REGIO = New System.Windows.Forms.Label()
        Me.ETIQUETA_RAÇA = New System.Windows.Forms.Label()
        Me.ETIQUETA_ID_EDIFICI = New System.Windows.Forms.Label()
        Me.ETIQUETA_REGIO_EDIFICI = New System.Windows.Forms.Label()
        Me.ETIQUETA_QUANTITAT_HABITANTS_EDIFICI = New System.Windows.Forms.Label()
        Me.ETIQUETA_CATEGORIA_EDIFICI = New System.Windows.Forms.Label()
        Me.ETIQUETA_NOM_GREMI = New System.Windows.Forms.Label()
        Me.ETIQUETA_REGIO_GREMI = New System.Windows.Forms.Label()
        Me.ETIQUETA_NOM_REGIO = New System.Windows.Forms.Label()
        Me.ETIQUETA_EXTENSIO_REGIO = New System.Windows.Forms.Label()
        Me.ETIQUETA_GREMIS_REGIO = New System.Windows.Forms.Label()
        Me.BUSCARGREMI = New System.Windows.Forms.Button()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.BUSCARREGIO = New System.Windows.Forms.Button()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.BUSCAREDIFICI = New System.Windows.Forms.Button()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.BackColor = System.Drawing.Color.Lime
        Me.Button1.Font = New System.Drawing.Font("Arial Black", 15.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.Button1.ForeColor = System.Drawing.SystemColors.MenuText
        Me.Button1.Location = New System.Drawing.Point(309, -1)
        Me.Button1.Name = "Button1"
        Me.Button1.Padding = New System.Windows.Forms.Padding(0, 0, 0, 3)
        Me.Button1.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Button1.Size = New System.Drawing.Size(144, 70)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "S.G ONU"
        Me.Button1.TextImageRelation = System.Windows.Forms.TextImageRelation.TextAboveImage
        Me.Button1.UseVisualStyleBackColor = False
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(163, 100)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(296, 20)
        Me.TextBox1.TabIndex = 6
        '
        'BUSCARHABITANT
        '
        Me.BUSCARHABITANT.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BUSCARHABITANT.Location = New System.Drawing.Point(8, 88)
        Me.BUSCARHABITANT.Name = "BUSCARHABITANT"
        Me.BUSCARHABITANT.Size = New System.Drawing.Size(151, 48)
        Me.BUSCARHABITANT.TabIndex = 7
        Me.BUSCARHABITANT.Text = "BUSCAR HABITANT"
        Me.BUSCARHABITANT.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(671, -1)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(133, 83)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 9
        Me.PictureBox1.TabStop = False
        '
        'ETIQUETA_EDAT
        '
        Me.ETIQUETA_EDAT.AutoSize = True
        Me.ETIQUETA_EDAT.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_EDAT.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_EDAT.Location = New System.Drawing.Point(268, 123)
        Me.ETIQUETA_EDAT.Name = "ETIQUETA_EDAT"
        Me.ETIQUETA_EDAT.Size = New System.Drawing.Size(40, 13)
        Me.ETIQUETA_EDAT.TabIndex = 10
        Me.ETIQUETA_EDAT.Text = "EDAT"
        '
        'ETIQUETA_GREMI
        '
        Me.ETIQUETA_GREMI.AutoSize = True
        Me.ETIQUETA_GREMI.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_GREMI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_GREMI.Location = New System.Drawing.Point(268, 147)
        Me.ETIQUETA_GREMI.Name = "ETIQUETA_GREMI"
        Me.ETIQUETA_GREMI.Size = New System.Drawing.Size(47, 13)
        Me.ETIQUETA_GREMI.TabIndex = 11
        Me.ETIQUETA_GREMI.Text = "GREMI"
        '
        'ETIQUETA_EDIFICI
        '
        Me.ETIQUETA_EDIFICI.AutoSize = True
        Me.ETIQUETA_EDIFICI.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_EDIFICI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_EDIFICI.Location = New System.Drawing.Point(349, 147)
        Me.ETIQUETA_EDIFICI.Name = "ETIQUETA_EDIFICI"
        Me.ETIQUETA_EDIFICI.Size = New System.Drawing.Size(51, 13)
        Me.ETIQUETA_EDIFICI.TabIndex = 12
        Me.ETIQUETA_EDIFICI.Text = "EDIFICI"
        '
        'ETIQUETA_GRUPSANGUINI
        '
        Me.ETIQUETA_GRUPSANGUINI.AutoSize = True
        Me.ETIQUETA_GRUPSANGUINI.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_GRUPSANGUINI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_GRUPSANGUINI.Location = New System.Drawing.Point(440, 123)
        Me.ETIQUETA_GRUPSANGUINI.Name = "ETIQUETA_GRUPSANGUINI"
        Me.ETIQUETA_GRUPSANGUINI.Size = New System.Drawing.Size(106, 13)
        Me.ETIQUETA_GRUPSANGUINI.TabIndex = 13
        Me.ETIQUETA_GRUPSANGUINI.Text = "GRUP SANGUINI"
        '
        'ETIQUETA_ID
        '
        Me.ETIQUETA_ID.AutoSize = True
        Me.ETIQUETA_ID.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_ID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_ID.Location = New System.Drawing.Point(165, 123)
        Me.ETIQUETA_ID.Name = "ETIQUETA_ID"
        Me.ETIQUETA_ID.Size = New System.Drawing.Size(20, 13)
        Me.ETIQUETA_ID.TabIndex = 14
        Me.ETIQUETA_ID.Text = "ID"
        '
        'ETIQUETA_REGIO
        '
        Me.ETIQUETA_REGIO.AutoSize = True
        Me.ETIQUETA_REGIO.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_REGIO.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_REGIO.Location = New System.Drawing.Point(185, 147)
        Me.ETIQUETA_REGIO.Name = "ETIQUETA_REGIO"
        Me.ETIQUETA_REGIO.Size = New System.Drawing.Size(46, 13)
        Me.ETIQUETA_REGIO.TabIndex = 15
        Me.ETIQUETA_REGIO.Text = "REGIO"
        '
        'ETIQUETA_RAÇA
        '
        Me.ETIQUETA_RAÇA.AutoSize = True
        Me.ETIQUETA_RAÇA.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_RAÇA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_RAÇA.Location = New System.Drawing.Point(349, 123)
        Me.ETIQUETA_RAÇA.Name = "ETIQUETA_RAÇA"
        Me.ETIQUETA_RAÇA.Size = New System.Drawing.Size(40, 13)
        Me.ETIQUETA_RAÇA.TabIndex = 16
        Me.ETIQUETA_RAÇA.Text = "RAÇA"
        '
        'ETIQUETA_ID_EDIFICI
        '
        Me.ETIQUETA_ID_EDIFICI.AutoSize = True
        Me.ETIQUETA_ID_EDIFICI.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_ID_EDIFICI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_ID_EDIFICI.Location = New System.Drawing.Point(537, 360)
        Me.ETIQUETA_ID_EDIFICI.Name = "ETIQUETA_ID_EDIFICI"
        Me.ETIQUETA_ID_EDIFICI.Size = New System.Drawing.Size(68, 13)
        Me.ETIQUETA_ID_EDIFICI.TabIndex = 17
        Me.ETIQUETA_ID_EDIFICI.Text = "ID EDIFICI"
        '
        'ETIQUETA_REGIO_EDIFICI
        '
        Me.ETIQUETA_REGIO_EDIFICI.AutoSize = True
        Me.ETIQUETA_REGIO_EDIFICI.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_REGIO_EDIFICI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_REGIO_EDIFICI.Location = New System.Drawing.Point(645, 391)
        Me.ETIQUETA_REGIO_EDIFICI.Name = "ETIQUETA_REGIO_EDIFICI"
        Me.ETIQUETA_REGIO_EDIFICI.Size = New System.Drawing.Size(46, 13)
        Me.ETIQUETA_REGIO_EDIFICI.TabIndex = 18
        Me.ETIQUETA_REGIO_EDIFICI.Text = "REGIO"
        '
        'ETIQUETA_QUANTITAT_HABITANTS_EDIFICI
        '
        Me.ETIQUETA_QUANTITAT_HABITANTS_EDIFICI.AutoSize = True
        Me.ETIQUETA_QUANTITAT_HABITANTS_EDIFICI.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_QUANTITAT_HABITANTS_EDIFICI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_QUANTITAT_HABITANTS_EDIFICI.Location = New System.Drawing.Point(620, 360)
        Me.ETIQUETA_QUANTITAT_HABITANTS_EDIFICI.Name = "ETIQUETA_QUANTITAT_HABITANTS_EDIFICI"
        Me.ETIQUETA_QUANTITAT_HABITANTS_EDIFICI.Size = New System.Drawing.Size(152, 13)
        Me.ETIQUETA_QUANTITAT_HABITANTS_EDIFICI.TabIndex = 19
        Me.ETIQUETA_QUANTITAT_HABITANTS_EDIFICI.Text = "QUANTITAT HABITANTS"
        '
        'ETIQUETA_CATEGORIA_EDIFICI
        '
        Me.ETIQUETA_CATEGORIA_EDIFICI.AutoSize = True
        Me.ETIQUETA_CATEGORIA_EDIFICI.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_CATEGORIA_EDIFICI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_CATEGORIA_EDIFICI.Location = New System.Drawing.Point(537, 391)
        Me.ETIQUETA_CATEGORIA_EDIFICI.Name = "ETIQUETA_CATEGORIA_EDIFICI"
        Me.ETIQUETA_CATEGORIA_EDIFICI.Size = New System.Drawing.Size(78, 13)
        Me.ETIQUETA_CATEGORIA_EDIFICI.TabIndex = 20
        Me.ETIQUETA_CATEGORIA_EDIFICI.Text = "CATEGORIA"
        '
        'ETIQUETA_NOM_GREMI
        '
        Me.ETIQUETA_NOM_GREMI.AutoSize = True
        Me.ETIQUETA_NOM_GREMI.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_NOM_GREMI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_NOM_GREMI.Location = New System.Drawing.Point(668, 210)
        Me.ETIQUETA_NOM_GREMI.Name = "ETIQUETA_NOM_GREMI"
        Me.ETIQUETA_NOM_GREMI.Size = New System.Drawing.Size(79, 13)
        Me.ETIQUETA_NOM_GREMI.TabIndex = 21
        Me.ETIQUETA_NOM_GREMI.Text = "NOM GREMI"
        '
        'ETIQUETA_REGIO_GREMI
        '
        Me.ETIQUETA_REGIO_GREMI.AutoSize = True
        Me.ETIQUETA_REGIO_GREMI.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_REGIO_GREMI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_REGIO_GREMI.Location = New System.Drawing.Point(521, 210)
        Me.ETIQUETA_REGIO_GREMI.Name = "ETIQUETA_REGIO_GREMI"
        Me.ETIQUETA_REGIO_GREMI.Size = New System.Drawing.Size(118, 13)
        Me.ETIQUETA_REGIO_GREMI.TabIndex = 22
        Me.ETIQUETA_REGIO_GREMI.Text = "REGIO DEL GREMI"
        '
        'ETIQUETA_NOM_REGIO
        '
        Me.ETIQUETA_NOM_REGIO.AutoSize = True
        Me.ETIQUETA_NOM_REGIO.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_NOM_REGIO.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_NOM_REGIO.Location = New System.Drawing.Point(278, 300)
        Me.ETIQUETA_NOM_REGIO.Name = "ETIQUETA_NOM_REGIO"
        Me.ETIQUETA_NOM_REGIO.Size = New System.Drawing.Size(78, 13)
        Me.ETIQUETA_NOM_REGIO.TabIndex = 23
        Me.ETIQUETA_NOM_REGIO.Text = "NOM REGIO"
        '
        'ETIQUETA_EXTENSIO_REGIO
        '
        Me.ETIQUETA_EXTENSIO_REGIO.AutoSize = True
        Me.ETIQUETA_EXTENSIO_REGIO.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_EXTENSIO_REGIO.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_EXTENSIO_REGIO.Location = New System.Drawing.Point(163, 278)
        Me.ETIQUETA_EXTENSIO_REGIO.Name = "ETIQUETA_EXTENSIO_REGIO"
        Me.ETIQUETA_EXTENSIO_REGIO.Size = New System.Drawing.Size(152, 13)
        Me.ETIQUETA_EXTENSIO_REGIO.TabIndex = 24
        Me.ETIQUETA_EXTENSIO_REGIO.Text = "EXTENSIO DE LA REGIO"
        '
        'ETIQUETA_GREMIS_REGIO
        '
        Me.ETIQUETA_GREMIS_REGIO.AutoSize = True
        Me.ETIQUETA_GREMIS_REGIO.BackColor = System.Drawing.Color.Gold
        Me.ETIQUETA_GREMIS_REGIO.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ETIQUETA_GREMIS_REGIO.Location = New System.Drawing.Point(321, 278)
        Me.ETIQUETA_GREMIS_REGIO.Name = "ETIQUETA_GREMIS_REGIO"
        Me.ETIQUETA_GREMIS_REGIO.Size = New System.Drawing.Size(138, 13)
        Me.ETIQUETA_GREMIS_REGIO.TabIndex = 25
        Me.ETIQUETA_GREMIS_REGIO.Text = "GREMIS DE LA REGIO"
        '
        'BUSCARGREMI
        '
        Me.BUSCARGREMI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BUSCARGREMI.Location = New System.Drawing.Point(363, 174)
        Me.BUSCARGREMI.Name = "BUSCARGREMI"
        Me.BUSCARGREMI.Size = New System.Drawing.Size(135, 45)
        Me.BUSCARGREMI.TabIndex = 26
        Me.BUSCARGREMI.Text = "BUSCAR NOM GREMI:"
        Me.BUSCARGREMI.UseVisualStyleBackColor = True
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(504, 187)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.Size = New System.Drawing.Size(281, 20)
        Me.TextBox2.TabIndex = 27
        '
        'BUSCARREGIO
        '
        Me.BUSCARREGIO.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BUSCARREGIO.Location = New System.Drawing.Point(8, 227)
        Me.BUSCARREGIO.Name = "BUSCARREGIO"
        Me.BUSCARREGIO.Size = New System.Drawing.Size(159, 48)
        Me.BUSCARREGIO.TabIndex = 28
        Me.BUSCARREGIO.Text = "BUSCAR NOM REGIO:"
        Me.BUSCARREGIO.UseVisualStyleBackColor = True
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(172, 242)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(281, 20)
        Me.TextBox3.TabIndex = 29
        '
        'BUSCAREDIFICI
        '
        Me.BUSCAREDIFICI.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle), System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BUSCAREDIFICI.Location = New System.Drawing.Point(352, 325)
        Me.BUSCAREDIFICI.Name = "BUSCAREDIFICI"
        Me.BUSCAREDIFICI.Size = New System.Drawing.Size(146, 42)
        Me.BUSCAREDIFICI.TabIndex = 30
        Me.BUSCAREDIFICI.Text = "BUSCAR ID EDIFICI:"
        Me.BUSCAREDIFICI.UseVisualStyleBackColor = True
        '
        'TextBox4
        '
        Me.TextBox4.Location = New System.Drawing.Point(504, 337)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(278, 20)
        Me.TextBox4.TabIndex = 31
        '
        'Button2
        '
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(540, 12)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(125, 40)
        Me.Button2.TabIndex = 32
        Me.Button2.Text = "AFEGIR DADES"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.HotTrack
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.TextBox4)
        Me.Controls.Add(Me.BUSCAREDIFICI)
        Me.Controls.Add(Me.TextBox3)
        Me.Controls.Add(Me.BUSCARREGIO)
        Me.Controls.Add(Me.TextBox2)
        Me.Controls.Add(Me.BUSCARGREMI)
        Me.Controls.Add(Me.ETIQUETA_GREMIS_REGIO)
        Me.Controls.Add(Me.ETIQUETA_EXTENSIO_REGIO)
        Me.Controls.Add(Me.ETIQUETA_NOM_REGIO)
        Me.Controls.Add(Me.ETIQUETA_REGIO_GREMI)
        Me.Controls.Add(Me.ETIQUETA_NOM_GREMI)
        Me.Controls.Add(Me.ETIQUETA_CATEGORIA_EDIFICI)
        Me.Controls.Add(Me.ETIQUETA_QUANTITAT_HABITANTS_EDIFICI)
        Me.Controls.Add(Me.ETIQUETA_REGIO_EDIFICI)
        Me.Controls.Add(Me.ETIQUETA_ID_EDIFICI)
        Me.Controls.Add(Me.ETIQUETA_RAÇA)
        Me.Controls.Add(Me.ETIQUETA_REGIO)
        Me.Controls.Add(Me.ETIQUETA_ID)
        Me.Controls.Add(Me.ETIQUETA_GRUPSANGUINI)
        Me.Controls.Add(Me.ETIQUETA_EDIFICI)
        Me.Controls.Add(Me.ETIQUETA_GREMI)
        Me.Controls.Add(Me.ETIQUETA_EDAT)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.BUSCARHABITANT)
        Me.Controls.Add(Me.TextBox1)
        Me.Controls.Add(Me.Button1)
        Me.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.Name = "Form1"
        Me.Text = "Id:"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents BUSCARHABITANT As Button
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents ETIQUETA_EDAT As Label
    Friend WithEvents ETIQUETA_GREMI As Label
    Friend WithEvents ETIQUETA_EDIFICI As Label
    Friend WithEvents ETIQUETA_GRUPSANGUINI As Label
    Friend WithEvents ETIQUETA_ID As Label
    Friend WithEvents ETIQUETA_REGIO As Label
    Friend WithEvents ETIQUETA_RAÇA As Label
    Friend WithEvents ETIQUETA_ID_EDIFICI As Label
    Friend WithEvents ETIQUETA_REGIO_EDIFICI As Label
    Friend WithEvents ETIQUETA_QUANTITAT_HABITANTS_EDIFICI As Label
    Friend WithEvents ETIQUETA_CATEGORIA_EDIFICI As Label
    Friend WithEvents ETIQUETA_NOM_GREMI As Label
    Friend WithEvents ETIQUETA_REGIO_GREMI As Label
    Friend WithEvents ETIQUETA_NOM_REGIO As Label
    Friend WithEvents ETIQUETA_EXTENSIO_REGIO As Label
    Friend WithEvents ETIQUETA_GREMIS_REGIO As Label
    Friend WithEvents BUSCARGREMI As Button
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents BUSCARREGIO As Button
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents BUSCAREDIFICI As Button
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents Button2 As Button
End Class
