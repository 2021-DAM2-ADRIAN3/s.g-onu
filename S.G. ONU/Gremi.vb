﻿Public Class Gremi
    'a la clase gremi ens podem trobar el nom i la regio on esta'
    Private NOM_GREMI As String
    Private REGIO_GREMI As String

    'creem el constructor'
    Public Sub New(ByVal NOM_GREMI As String, ByVal REGIO_GREMI As String)
        Me.NOM_GREMI = NOM_GREMI
        Me.REGIO_GREMI = REGIO_GREMI
    End Sub
    'creem getters i seters'
    Public Sub setNOM_GREMI(ByVal NOM_GREMI As String)
        Me.NOM_GREMI = NOM_GREMI
    End Sub

    Public Function getNOM_GREMI() As String
        Return Me.NOM_GREMI
    End Function

    Public Sub setREGIO_GREMI(ByVal REGIO_GREMI As String)
        Me.REGIO_GREMI = REGIO_GREMI
    End Sub

    Public Function getREGIO_GREMI() As String
        Return Me.REGIO_GREMI
    End Function

End Class
