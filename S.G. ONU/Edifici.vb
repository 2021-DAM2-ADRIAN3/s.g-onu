﻿Public Class Edifici
    'a la clase edifici ens trobem amb el id de l'edifici, la regio on esta, la quantitat d'habitants que te i la categoria a la que pertany'
    Private ID_EDIFICI As String
    Private REGIO_EDIFICI As String
    Private QUANTITAT_HABITANTS_EDIFICI As Int32
    Private CATEGORIA_EDIFICI As Int32

    'creem el constructor'
    Public Sub New(ByVal ID_EDIFICI As String, ByVal REGIO_EDIFICI As String, ByVal QUANTITAT_HABITATS_EDIFICI As Int32, ByVal CATEGORIA_EDIFICI As Int32)
        Me.ID_EDIFICI = ID_EDIFICI
        Me.REGIO_EDIFICI = REGIO_EDIFICI
        Me.QUANTITAT_HABITANTS_EDIFICI = QUANTITAT_HABITANTS_EDIFICI
        Me.CATEGORIA_EDIFICI = CATEGORIA_EDIFICI
    End Sub
    'creem getters i seters'
    Public Sub setID_EDIFICI(ByVal ID_EDIFICI As String)
        Me.ID_EDIFICI = ID_EDIFICI
    End Sub

    Public Function getID_EDIFICI() As String
        Return Me.ID_EDIFICI
    End Function

    Public Sub setREGIO_EDIFICI(ByVal REGIO_EDIFICI As String)
        Me.REGIO_EDIFICI = REGIO_EDIFICI
    End Sub

    Public Function getREGIO_EDIFICI() As String
        Return Me.REGIO_EDIFICI
    End Function

    Public Sub setQUANTITAT_HABITANTS_EDIFICI(ByVal QUANTITAT_HABITANTS_EDIFICI As Int32)
        Me.QUANTITAT_HABITANTS_EDIFICI = QUANTITAT_HABITANTS_EDIFICI
    End Sub

    Public Function getQUANTITAT_HABITANTS_EDIFICI() As Int32
        Return Me.QUANTITAT_HABITANTS_EDIFICI
    End Function

    Public Sub setCATEGORIA_EDIFICI(ByVal CATEGORIA_EDIFICI As Int32)
        Me.CATEGORIA_EDIFICI = CATEGORIA_EDIFICI
    End Sub

    Public Function getCATEGORIA_EDIFICI() As Int32
        Return Me.CATEGORIA_EDIFICI
    End Function

End Class

